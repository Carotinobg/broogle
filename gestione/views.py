from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from .models import *
from django.views.generic.base import TemplateView
from django.urls.base import reverse_lazy
from django.contrib import messages
from .baseviews import *
from .aiace import *

"""
Cose da fare:
- form di carico dinamica
- form di scarico dinamica
- lista carichi per anagrafica
- lista scarichi per anagrafica
=> Alla luce di queste ultime due righe, magari servirebbe rivedere il menu: lasciare le voci "grandi" sopra e poi, quando si clicca, sotto compaiono le varie sottovoci
"""

class IndexView(TemplateView):
    template_name = 'gestione/index.html'
    

# PRODOTTO
class MagazzinoView(generic.ListView):
    model = Prodotto
    template_name = 'gestione/prodotto_list.html'    
    context_object_name = 'prodotti'
    
class CreaProdottoView(generic.CreateView):
    model = Prodotto
    template_name = 'gestione/prodotto_create.html'
    fields = '__all__'    
    
    def get_success_url(self):
        messages.info(self.request, "Prodotto %s creato" % self.object)
        return reverse_lazy('gestione:magazzino')
    
class ProdottoView(generic.UpdateView):
    model = Prodotto
    template_name = 'gestione/prodotto_update.html'
    fields = '__all__'
    
    def get_success_url(self):        
        messages.info(self.request, "Prodotto %s modificato" % self.object)        
        return reverse_lazy('gestione:magazzino')
    
# CATEGORIE
class CategorieView(generic.ListView):
    model = Categoria
    template_name = 'gestione/categoria_list.html'    
    context_object_name = 'categorie'
    
class CreaCategoriaView(generic.CreateView):
    model = Categoria
    template_name = 'gestione/categoria_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Categoria %s creata" % self.object)
        return reverse_lazy('gestione:categorie')
    
class CategoriaView(generic.UpdateView):
    model = Categoria
    template_name = 'gestione/categoria_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Categoria %s modificata" % self.object)
        return reverse_lazy('gestione:categorie')

# TIPO ANAGRAFICA
class TipoAnagraficheView(generic.ListView):
    model = TipoAnagrafica
    template_name = 'gestione/tipoanagrafica_list.html'    
    context_object_name = 'tipoanagrafiche'
    
class CreaTipoAnagraficaView(generic.CreateView):
    model = TipoAnagrafica
    template_name = 'gestione/tipoanagrafica_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Tipo Anagrafica %s creato" % self.object)
        return reverse_lazy('gestione:tipoanagrafiche')
    
class TipoAnagraficaView(generic.UpdateView):
    model = TipoAnagrafica
    template_name = 'gestione/tipoanagrafica_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Tipo Anagrafica %s modificato" % self.object)
        return reverse_lazy('gestione:tipoanagrafiche')

# Anagrafica
class AnagraficheView(generic.ListView):
    model = Anagrafica
    template_name = 'gestione/anagrafica_list.html'    
    context_object_name = 'anagrafiche'
    
class CreaAnagraficaView(generic.CreateView):
    model = Anagrafica
    template_name = 'gestione/anagrafica_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Anagrafica %s creata" % self.object)
        return reverse_lazy('gestione:anagrafiche')
    
class AnagraficaView(generic.UpdateView):
    model = Anagrafica
    template_name = 'gestione/anagrafica_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Anagrafica %s modificata" % self.object)
        return reverse_lazy('gestione:anagrafiche')

# RICARICO
class RicarichiView(generic.ListView):
    model = Ricarico
    template_name = 'gestione/ricarico_list.html'    
    context_object_name = 'ricarichi'
    
class CreaRicaricoView(generic.CreateView):
    model = Ricarico
    template_name = 'gestione/ricarico_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Ricarico %s creato" % self.object)
        return reverse_lazy('gestione:ricarichi')
    
class RicaricoView(generic.UpdateView):
    model = Ricarico
    template_name = 'gestione/ricarico_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Ricarico %s modificato" % self.object)
        return reverse_lazy('gestione:ricarichi')

# IVA
class ListaIVAView(generic.ListView):
    model = Iva
    template_name = 'gestione/iva_list.html'    
    context_object_name = 'iva'
    
class CreaIVAView(generic.CreateView):
    model = Iva
    template_name = 'gestione/iva_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "IVA %s creata" % self.object)
        return reverse_lazy('gestione:listaiva')
    
class IVAView(generic.UpdateView):
    model = Iva
    template_name = 'gestione/iva_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "IVA %s modificata" % self.object)
        return reverse_lazy('gestione:listaiva')


# CARICO
class CarichiView(generic.ListView):
    model = Carico
    template_name = 'gestione/carico_list.html'    
    context_object_name = 'carichi'
    
class CreaCaricoView(generic.CreateView):
    model = Carico
    template_name = 'gestione/carico_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Carico %s creato" % self.object)
        return reverse_lazy('gestione:carichi')
    
class CreaCaricoDynView(generic.TemplateView):
    """
    Ecco come deve essere.
    Aggiungo una riga.
    Nella riga inserisco il codice
    Il codice autocompleta sia la riga che la descrizione
    Alla fine dell'inserimento devo validare tutte le righe
    Se qualche prodotto non c'è, comunico all'utente: i prodotti tale e tale non ci sono, li creo?
    Se sceglie sì, creo i prodotti
    Se sceglie no, ritorno alla modifica    
    """
    template_name = 'gestione/carico_dyn_create.html'
    
class CaricoView(generic.UpdateView):
    model = Carico
    template_name = 'gestione/carico_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Carico %s modificato" % self.object)
        return reverse_lazy('gestione:carichi')
   
class DettaglioCaricoView(DettaglioView):
    model = Carico
    submodel = VoceCarico
    template_name = 'gestione/carico_dettaglio.html'
    fields = '__all__'    

# VOCI DI CARICO
class VociCaricoView(generic.ListView):
    model = VoceCarico
    template_name = 'gestione/vocecarico_list.html'    
    context_object_name = 'vocicarico'
    
class CreaVoceCaricoView(generic.CreateView):
    model = VoceCarico
    template_name = 'gestione/vocecarico_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Voce di Carico %s creata" % self.object)
        return reverse_lazy('gestione:vocicarico')
    
class VoceCaricoView(generic.UpdateView):
    model = VoceCarico
    template_name = 'gestione/vocecarico_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Voce di Carico %s modificata" % self.object)
        return reverse_lazy('gestione:vocicarico')

class DettaglioScaricoView(DettaglioView):
    model = Scarico
    submodel = VoceScarico
    template_name = 'gestione/scarico_dettaglio.html'
    fields = '__all__'
    
    def getVoci(self, idCarico):
        print("Ma qui non ci entro?")
        return self.submodel.objects.filter(scarico = idCarico)

# SCARICO
class ScarichiView(generic.ListView):
    model = Scarico
    template_name = 'gestione/scarico_list.html'    
    context_object_name = 'scarichi'
    
class CreaScaricoView(generic.CreateView):
    model = Scarico
    template_name = 'gestione/scarico_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Scarico %s creato" % self.object)
        return reverse_lazy('gestione:scarichi')
    
class ScaricoView(generic.UpdateView):
    model = Scarico
    template_name = 'gestione/scarico_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Scarico %s modificato" % self.object)
        return reverse_lazy('gestione:scarichi')
    
class CreaScaricoDynView(generic.TemplateView):
    """
    Ecco come deve essere.
    Aggiungo una riga.
    Nella riga inserisco il codice
    Il codice autocompleta sia la riga che la descrizione
    Alla fine dell'inserimento devo validare tutte le righe
    Se qualche prodotto non c'è, comunico all'utente: i prodotti tale e tale non ci sono, li creo?
    Se sceglie sì, creo i prodotti
    Se sceglie no, ritorno alla modifica    
    """
    template_name = 'gestione/scarico_dyn_create.html'

# VOCI DI SCARICO
class VociScaricoView(generic.ListView):
    model = VoceScarico
    template_name = 'gestione/vocescarico_list.html'    
    context_object_name = 'vociscarico'
    
class CreaVoceScaricoView(generic.CreateView):
    model = VoceScarico
    template_name = 'gestione/vocescarico_create.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Voce di Scarico %s creata" % self.object)
        return reverse_lazy('gestione:vociscarico')
    
class VoceScaricoView(generic.UpdateView):
    model = VoceScarico
    template_name = 'gestione/vocescarico_update.html'
    fields = '__all__'
    
    def get_success_url(self):
        messages.info(self.request, "Voce di Scarico %s modificata" % self.object)
        return reverse_lazy('gestione:vociscarico')
    
def caricaProdotti(request):
    
    if request.method == "POST":
        diz = request.POST.dict()
        print(diz)
        
        data = datetime.strftime(datetime.strptime(diz['data'], "%d/%m/%Y"), "%Y-%m-%d")        
        fornitore = diz['fornitore']
        
        # Creo un carico coi dati testé fornitimi        
        carico = Carico.objects.create(data=data, fornitore = Anagrafica.objects.get(nome = fornitore))
        carico.save()
        
        print(data, fornitore)
        
        indice = 0
        try:
            while True:
                i = str(indice)
                codice = diz['id_codice' + i]
                desc = diz['id_prodotto' + i]
                quantita = int(diz['id_quantita' + i])
                prezzo = int(diz['id_prezzo' + i])
                totale = quantita * prezzo
                
                print("codice:", codice, "desc:",desc, "quantita:", quantita, "prezzo:", prezzo, "totale:", totale)
                voce = VoceCarico.objects.create(prodotto = Prodotto.objects.get(codice=codice), carico = carico, quantita = quantita, prezzo = prezzo);
                voce.save()
                
                prodotto = Prodotto.objects.get(codice = codice)
                prodotto.disponibilita = prodotto.disponibilita + quantita
                prodotto.save()
                
                indice += 1
        except Exception:
            pass
    
#     return HttpResponse("")
    return HttpResponseRedirect(reverse_lazy('gestione:dettagliocarico', kwargs={'pk': carico.pk}))


def scaricaProdotti(request):
    
    if request.method == "POST":
        diz = request.POST.dict()
        print(diz)
        
        data = datetime.strftime(datetime.strptime(diz['data'], "%d/%m/%Y"), "%Y-%m-%d")        
        cliente = diz['cliente']
        
        # Creo uno scarico coi dati testé fornitimi        
        scarico = Scarico.objects.create(data=data, cliente = Anagrafica.objects.get(nome = cliente))
        scarico.save()
        
        print(data, cliente)
        
        indice = 0
        try:
            while True:
                i = str(indice)
                codice = diz['id_codice' + i]
                desc = diz['id_prodotto' + i]
                quantita = int(diz['id_quantita' + i])
                prezzo = int(diz['id_prezzo' + i])
                totale = quantita * prezzo
                
                print("codice:", codice, "desc:",desc, "quantita:", quantita, "prezzo:", prezzo, "totale:", totale)
                
                print("Creo la voce di scarico")
                voce = VoceScarico.objects.create(prodotto = Prodotto.objects.get(codice=codice), scarico = scarico, quantita = quantita, prezzo = prezzo);
                
                print("La salvo")
                voce.save()
                
                prodotto = Prodotto.objects.get(codice = codice)
                prodotto.disponibilita = prodotto.disponibilita - quantita
                prodotto.save()
                
                indice += 1
        except Exception as e:
            print(str(e))
            pass
    
#     return HttpResponse("")
    return HttpResponseRedirect(reverse_lazy('gestione:dettaglioscarico', kwargs={'pk': scarico.pk}))
