from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.views import generic
from .models import *
from django.core import serializers
import json
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime
from django.urls.base import reverse_lazy

# TOOLS
def listaFornitori(request):
    serializzato = ""
    
    cercanda = None
    if request.method == 'GET' and 'Fornitore' in request.GET:
        cercanda = request.GET['Fornitore']
    
    if cercanda:        
        idFornitore = TipoAnagrafica.objects.get(tipo="FORNITORE").id
        fornitori = Anagrafica.objects.filter(tipo=idFornitore).filter(nome__icontains=cercanda).values('id', 'nome')
#         print(fornitori)
        serializzato = json.dumps(list(fornitori), cls=DjangoJSONEncoder)        

#     print("Cercanda:", cercanda)
#     print(serializzato)
    return HttpResponse(serializzato)

def listaClienti(request):
    serializzato = ""
    
    cercanda = None
    if request.method == 'GET' and 'Cliente' in request.GET:
        cercanda = request.GET['Cliente']
    
    if cercanda:        
        idCliente = TipoAnagrafica.objects.get(tipo="CLIENTE").id
        clienti = Anagrafica.objects.filter(tipo=idCliente).filter(nome__icontains=cercanda).values('id', 'nome')
#         print(clienti)
        serializzato = json.dumps(list(clienti), cls=DjangoJSONEncoder)        

#     print("Cercanda:", cercanda)
#     print(serializzato)
    return HttpResponse(serializzato)

def listaProdottiCodice(request):
    serializzato = ""
    
    cercanda = None
    if request.method == 'GET' and 'Codice' in request.GET:
        cercanda = request.GET['Codice']
    
#     print("Cercanda:", cercanda)
    
    if cercanda:  
        prodotti = Prodotto.objects.filter(codice__icontains=cercanda).values()
        print(prodotti)
        serializzato = json.dumps(list(prodotti), cls=DjangoJSONEncoder)        
    
#     print(serializzato)
    return HttpResponse(serializzato)

def listaProdottiDescrizione(request):
    serializzato = ""
    
    cercanda = None
    if request.method == 'GET' and 'Descrizione' in request.GET:
        cercanda = request.GET['Descrizione']
    
    if cercanda:
        prodotti = Prodotto.objects.filter(descrizione__icontains=cercanda).values()         
        serializzato = json.dumps(list(prodotti), cls=DjangoJSONEncoder)        
    
    return HttpResponse(serializzato)

def validaListaProdotti(request):
    serializzato = ""
    if request.method == "POST":
        diz = request.POST.dict()
        lista = json.loads(diz['lista'])
        risposta = []
        for e in lista:
            codice = e['codice']
            desc = e['desc']
            print("Codice = ", codice, ";descrizione = ", desc)
            try:
                Prodotto.objects.get(codice=codice, descrizione=desc)
                #risposta.append({'codice': codice, 'esiste': 'OK'})
            except Prodotto.DoesNotExist:
                print("Il prodotto non esiste")
                risposta.append({'codice': codice, 'desc': desc, 'esiste': 'KO'})
            
            serializzato = json.dumps(risposta, cls=DjangoJSONEncoder)
    else:
        pass
        #print("non c'è nessun post")
    
    return HttpResponse(serializzato)
       

