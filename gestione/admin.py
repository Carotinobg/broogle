from django.contrib import admin

# Register your models here.
from .models import Anagrafica, Carico, Scarico, Ricarico, Prodotto, TipoAnagrafica, Iva, VoceCarico, VoceScarico, Categoria

admin.site.register(Anagrafica)
admin.site.register(Carico)
admin.site.register(Scarico)
admin.site.register(Ricarico)
admin.site.register(Prodotto)
admin.site.register(TipoAnagrafica)
admin.site.register(Iva)
admin.site.register(VoceScarico)
admin.site.register(VoceCarico)
admin.site.register(Categoria)
