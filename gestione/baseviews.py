from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.views.generic.base import TemplateView
from django.urls.base import reverse_lazy
from django.contrib import messages
from .models import *

class DettaglioView(generic.TemplateView):
#     model = Carico
#     template_name = 'gestione/carico_dettaglio.html'
    fields = '__all__'
    nomeVar = "carico"
    
    def getVoci(self, idCarico):
        return self.submodel.objects.filter(carico = idCarico)
    
    def get_context_data(self, **kwargs):
        context = super(DettaglioView, self).get_context_data(**kwargs)
        idCarico = self.kwargs['pk']
#         print(idCarico)
        context['Nome'] = self.model.objects.get(id = idCarico)
        context['Voci'] = []
        voci = self.getVoci(idCarico)
        for v in voci:
            v.unitaMisura = Prodotto.objects.get(id = v.prodotto.id).unitaMisura
            context['Voci'].append(v)            
#             print(v)
            
        
        return context;