from django.db import models

# Create your models here.

class Categoria(models.Model):
    descrizione = models.CharField(max_length = 64)
    
    def __str__(self):
        return self.descrizione

class Prodotto(models.Model):
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    codice = models.CharField(max_length = 64)
    descrizione = models.CharField(max_length = 64, blank = True, null = True)
    unitaMisura = models.CharField(max_length = 16)   
    disponibilita = models.IntegerField()
    prezzo = models.FloatField()
    
    def __str__(self):
        return self.codice

class TipoAnagrafica(models.Model):
    tipo = models.CharField(max_length = 16)
    
    def __str__(self):
        return self.tipo
    
class Anagrafica(models.Model):
    nome = models.CharField(max_length = 256)
    piva = models.CharField(max_length = 16, blank = True, null = True)
    indirizzo = models.CharField(max_length = 64, blank = True, null = True)
    citta = models.CharField(max_length = 64, blank = True, null = True)
    cap = models.CharField(max_length = 5, blank = True, null = True)
    provincia = models.CharField(max_length = 2, blank = True, null = True)
    telefono = models.CharField(max_length = 32, blank = True, null = True)
    cellulare = models.CharField(max_length = 32, blank = True, null = True)
    fax = models.CharField(max_length = 32, blank = True, null = True)
    mail = models.CharField(max_length = 64, blank = True, null = True) 
    tipo = models.ForeignKey(TipoAnagrafica, on_delete=models.CASCADE)
    
    def __str__(self):
        return "%s - %s" % (self.nome, self.tipo)
    
class Ricarico(models.Model):
    nome = models.CharField(max_length = 16)
    percentuale = models.FloatField()
    
    def __str__(self):
        return self.nome

class Iva(models.Model):
    nome = models.CharField(max_length = 16)
    percentuale = models.FloatField()
    
    def __str__(self):
        return self.nome
    
class Carico(models.Model):
    data = models.DateField()
    fornitore = models.ForeignKey(Anagrafica, null = True, blank = True)
    
    def __str__(self):
        return '%s %s' % (self.data, self.fornitore)
    
class VoceCarico(models.Model):
    prodotto = models.ForeignKey(Prodotto)
    carico = models.ForeignKey(Carico)
    quantita = models.IntegerField()    
    prezzo = models.FloatField()
    
    def __str__(self):
        return '%s %s' % (self.prodotto, self.carico)
     
class Scarico(models.Model):
    data = models.DateTimeField()
    cliente = models.ForeignKey(Anagrafica, null = True, blank = True)
    
    def __str__(self):
        return '%s %s' % (self.data, self.cliente)
    
class VoceScarico(models.Model):
    prodotto = models.ForeignKey(Prodotto)
    scarico = models.ForeignKey(Scarico)
    quantita = models.IntegerField()    
    prezzo = models.FloatField()
    
    def __str__(self):
        return '%s %s' % (self.prodotto, self.scarico)
    
