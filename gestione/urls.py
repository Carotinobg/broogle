from django.conf.urls import url
from . import views
from . import aiace

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="indice"),
    
    url(r'^magazzino/$', views.MagazzinoView.as_view(), name="magazzino"),
    url(r'^creaprodotto/$', views.CreaProdottoView.as_view(), name="creaprodotto"),
    url(r'^prodotto/(?P<pk>[0-9]+)$', views.ProdottoView.as_view(), name="prodotto"),
    
    url(r'^categorie/$', views.CategorieView.as_view(), name="categorie"),
    url(r'^creacategoria/$', views.CreaCategoriaView.as_view(), name="creacategoria"),
    url(r'^categoria/(?P<pk>[0-9]+)$', views.CategoriaView.as_view(), name="categoria"),
    
    url(r'^tipoanagrafiche/$', views.TipoAnagraficheView.as_view(), name="tipoanagrafiche"),
    url(r'^creatipoanagrafica/$', views.CreaTipoAnagraficaView.as_view(), name="creatipoanagrafica"),
    url(r'^tipoanagrafica/(?P<pk>[0-9]+)$', views.TipoAnagraficaView.as_view(), name="tipoanagrafica"),
    
    url(r'^anagrafiche/$', views.AnagraficheView.as_view(), name="anagrafiche"),
    url(r'^creaanagrafica/$', views.CreaAnagraficaView.as_view(), name="creaanagrafica"),
    url(r'^anagrafica/(?P<pk>[0-9]+)$', views.AnagraficaView.as_view(), name="anagrafica"),
    
    url(r'^ricarichi/$', views.RicarichiView.as_view(), name="ricarichi"),
    url(r'^crearicarico/$', views.CreaRicaricoView.as_view(), name="crearicarico"),
    url(r'^ricarico/(?P<pk>[0-9]+)$', views.RicaricoView.as_view(), name="ricarico"),
    
    url(r'^listaiva/$', views.ListaIVAView.as_view(), name="listaiva"),
    url(r'^creaiva/$', views.CreaIVAView.as_view(), name="creaiva"),
    url(r'^iva/(?P<pk>[0-9]+)$', views.IVAView.as_view(), name="iva"),
    
    url(r'^carichi/$', views.CarichiView.as_view(), name="carichi"),
    url(r'^creacarico/$', views.CreaCaricoView.as_view(), name="creacarico"),
    url(r'^creacaricodyn/$', views.CreaCaricoDynView.as_view(), name="creacaricodyn"),
    url(r'^caricaprodotti/$', views.caricaProdotti, name="caricaprodotti"),
    
    url(r'^carico/(?P<pk>[0-9]+)$', views.CaricoView.as_view(), name="carico"),
    url(r'^dettagliocarico/(?P<pk>[0-9]+)$', views.DettaglioCaricoView.as_view(), name="dettagliocarico"),
    
    url(r'^vocicarico/$', views.VociCaricoView.as_view(), name="vocicarico"),
    url(r'^creavocecarico/$', views.CreaVoceCaricoView.as_view(), name="creavocecarico"),
    url(r'^vocecarico/(?P<pk>[0-9]+)$', views.VoceCaricoView.as_view(), name="vocecarico"),
    
    url(r'^scarichi/$', views.ScarichiView.as_view(), name="scarichi"),
    url(r'^creascarico/$', views.CreaScaricoView.as_view(), name="creascarico"),
    url(r'^creascaricodyn/$', views.CreaScaricoDynView.as_view(), name="creascaricodyn"),
    url(r'^scaricaprodotti/$', views.scaricaProdotti, name="scaricaprodotti"),
    
    url(r'^scarico/(?P<pk>[0-9]+)$', views.ScaricoView.as_view(), name="scarico"),
    url(r'^dettaglioscarico/(?P<pk>[0-9]+)$', views.DettaglioScaricoView.as_view(), name="dettaglioscarico"),
    
    url(r'^vociscarico/$', views.VociScaricoView.as_view(), name="vociscarico"),
    url(r'^creavocescarico/$', views.CreaVoceScaricoView.as_view(), name="creavocescarico"),
    url(r'^vocescarico/(?P<pk>[0-9]+)$', views.VoceScaricoView.as_view(), name="vocescarico"),
    
    url(r'^listafornitori/$', aiace.listaFornitori, name="listafornitori"),
    url(r'^listaclienti/$', aiace.listaClienti, name="listaclienti"),
    url(r'^listaprodotticodice/$', aiace.listaProdottiCodice, name="listaprodotticodice"),
    url(r'^listaprodottidescrizione/$', aiace.listaProdottiDescrizione, name="listaprodottidescrizione"),
    url(r'^validalistaprodotti/$', aiace.validaListaProdotti, name="validalistaprodotti"),
    
]
